﻿# TODO: Translation updated at 2019-07-20 11:48

# game/script.rpy:283
translate brazilian konami_code_e5b259c9:

    # "Max le Fou" "What the heck am I doing here?..."
    "Max le Fou" "What the heck am I doing here?..."

# game/script.rpy:291
translate brazilian update_menu_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# game/script.rpy:305
translate brazilian gjconnect_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# game/script.rpy:329
translate brazilian gjconnect_4ae60ea0:

    # "Disconnected."
    "Disconnected."

# game/script.rpy:358
translate brazilian gjconnect_e254a406:

    # "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."
    "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."

translate brazilian strings:

    # game/script.rpy:13
    old "{size=80}Thanks for playing!"
    new "{size=80}Thanks for playing!"

    # game/script.rpy:193
    old "Sakura"
    new "Sakura"

    # game/script.rpy:194
    old "Rika"
    new "Rika"

    # game/script.rpy:195
    old "Nanami"
    new "Nanami"

    # game/script.rpy:196
    old "Sakura's mom"
    new "Sakura's mom"

    # game/script.rpy:197
    old "Sakura's dad"
    new "Sakura's dad"

    # game/script.rpy:198
    old "Toshio"
    new "Toshio"

    # game/script.rpy:203
    old "Taichi"
    new "Taichi"

    # game/script.rpy:215
    old "Master"
    new "Master"

    # game/script.rpy:216
    old "Big brother"
    new "Big brother"

    # game/script.rpy:227
    old "Achievement obtained!"
    new "Achievement obtained!"

    # game/script.rpy:256
    old "Please enter your name and press Enter:"
    new "Please enter your name and press Enter:"

    # game/script.rpy:314
    old "Disconnect from Game Jolt?"
    new "Disconnect from Game Jolt?"

    # game/script.rpy:314
    old "Yes, disconnect."
    new "Yes, disconnect."

    # game/script.rpy:314
    old "No, return to menu."
    new "No, return to menu."

    # game/script.rpy:376
    old "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"
    new "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"

    # game/script.rpy:376
    old "Yes, try again."
    new "Yes, try again."

    # game/script.rpy:394
    old "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    new "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"

