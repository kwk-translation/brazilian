﻿# TODO: Translation updated at 2019-07-20 11:48

translate brazilian strings:

    # game/omake.rpy:95
    old "Opening song \"TAKE MY HEART\""
    new "Opening song \"TAKE MY HEART\""

    # game/omake.rpy:96
    old "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"
    new "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"

    # game/omake.rpy:102
    old "{b}Japanese lyrics:{/b}\n"
    new "{b}Japanese lyrics:{/b}\n"

    # game/omake.rpy:118
    old "{b}Romaji:{/b}\n"
    new "{b}Romaji:{/b}\n"

    # game/omake.rpy:134
    old "{b}Translation:{/b}\n"
    new "{b}Translation:{/b}\n"

    # game/omake.rpy:135
    old "When you take my hand, I feel I could fly"
    new "When you take my hand, I feel I could fly"

    # game/omake.rpy:136
    old "When I dive into your eyes, I feel I could drown in happiness\n"
    new "When I dive into your eyes, I feel I could drown in happiness\n"

    # game/omake.rpy:137
    old "We sure look different"
    new "We sure look different"

    # game/omake.rpy:138
    old "But despite that, my heart beats loud\n"
    new "But despite that, my heart beats loud\n"

    # game/omake.rpy:139
    old "A boy and a girl"
    new "A boy and a girl"

    # game/omake.rpy:140
    old "I'm just a human"
    new "I'm just a human"

    # game/omake.rpy:141
    old "I hope you don't mind"
    new "I hope you don't mind"

    # game/omake.rpy:142
    old "I can't control my feelings"
    new "I can't control my feelings"

    # game/omake.rpy:143
    old "Come to me, take my heart"
    new "Come to me, take my heart"

    # game/omake.rpy:144
    old "I will devote myself to you"
    new "I will devote myself to you"

    # game/omake.rpy:145
    old "No matter what, I love you"
    new "No matter what, I love you"

    # game/omake.rpy:166
    old "{b}D.O.B.:{/b} 1978/09/29\n"
    new "{b}D.O.B.:{/b} 1978/09/29\n"

    # game/omake.rpy:167
    old "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"
    new "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"

    # game/omake.rpy:168
    old "{b}Height:{/b} 5.4ft\n"
    new "{b}Height:{/b} 5.4ft\n"

    # game/omake.rpy:169
    old "{b}Weight:{/b} 136 pounds\n"
    new "{b}Weight:{/b} 136 pounds\n"

    # game/omake.rpy:170
    old "{b}Measurements:{/b} 74-64-83\n"
    new "{b}Measurements:{/b} 74-64-83\n"

    # game/omake.rpy:171
    old "{b}Blood type:{/b} A\n"
    new "{b}Blood type:{/b} A\n"

    # game/omake.rpy:172
    old "{b}Favourite manga:{/b} High School Samurai\n"
    new "{b}Favourite manga:{/b} High School Samurai\n"

    # game/omake.rpy:173
    old "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"
    new "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"

    # game/omake.rpy:174
    old "{b}Favourite food:{/b} American hamburgers\n"
    new "{b}Favourite food:{/b} American hamburgers\n"

    # game/omake.rpy:175
    old "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."
    new "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."

    # game/omake.rpy:193
    old "{b}D.O.B.:{/b} 1979/02/28\n"
    new "{b}D.O.B.:{/b} 1979/02/28\n"

    # game/omake.rpy:194
    old "{b}P.O.B.:{/b} Kameoka, Kyoto\n"
    new "{b}P.O.B.:{/b} Kameoka, Kyoto\n"

    # game/omake.rpy:195
    old "{b}Height:{/b} 5.1ft\n"
    new "{b}Height:{/b} 5.1ft\n"

    # game/omake.rpy:196
    old "{b}Weight:{/b} 121 pounds\n"
    new "{b}Weight:{/b} 121 pounds\n"

    # game/omake.rpy:197
    old "{b}Measurements:{/b} Unknown\n"
    new "{b}Measurements:{/b} Unknown\n"

    # game/omake.rpy:198
    old "{b}Blood type:{/b} AB\n"
    new "{b}Blood type:{/b} AB\n"

    # game/omake.rpy:199
    old "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"
    new "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"

    # game/omake.rpy:200
    old "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"
    new "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"

    # game/omake.rpy:201
    old "{b}Favourite food:{/b} Beef yakitori\n"
    new "{b}Favourite food:{/b} Beef yakitori\n"

    # game/omake.rpy:202
    old "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."
    new "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."

    # game/omake.rpy:225
    old "{b}D.O.B.:{/b} 1979/08/05\n"
    new "{b}D.O.B.:{/b} 1979/08/05\n"

    # game/omake.rpy:226
    old "{b}P.O.B.:{/b} The Village, Osaka\n"
    new "{b}P.O.B.:{/b} The Village, Osaka\n"

    # game/omake.rpy:227
    old "{b}Height:{/b} 5ft\n"
    new "{b}Height:{/b} 5ft\n"

    # game/omake.rpy:228
    old "{b}Weight:{/b} 110 pounds\n"
    new "{b}Weight:{/b} 110 pounds\n"

    # game/omake.rpy:229
    old "{b}Measurements:{/b} 92-64-87\n"
    new "{b}Measurements:{/b} 92-64-87\n"

    # game/omake.rpy:230
    old "{b}Blood type:{/b} O\n"
    new "{b}Blood type:{/b} O\n"

    # game/omake.rpy:231
    old "{b}Favourite manga:{/b} Rosario Maiden\n"
    new "{b}Favourite manga:{/b} Rosario Maiden\n"

    # game/omake.rpy:232
    old "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"
    new "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"

    # game/omake.rpy:233
    old "{b}Favourite food:{/b} Takoyaki\n"
    new "{b}Favourite food:{/b} Takoyaki\n"

    # game/omake.rpy:234
    old "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."
    new "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."

    # game/omake.rpy:257
    old "{b}D.O.B.:{/b} 1980/10/11\n"
    new "{b}D.O.B.:{/b} 1980/10/11\n"

    # game/omake.rpy:258
    old "{b}P.O.B.:{/b} Ginoza, Okinawa\n"
    new "{b}P.O.B.:{/b} Ginoza, Okinawa\n"

    # game/omake.rpy:259
    old "{b}Height:{/b} 4.5ft\n"
    new "{b}Height:{/b} 4.5ft\n"

    # game/omake.rpy:260
    old "{b}Weight:{/b} 99 pounds\n"
    new "{b}Weight:{/b} 99 pounds\n"

    # game/omake.rpy:261
    old "{b}Measurements:{/b} 71-51-74\n"
    new "{b}Measurements:{/b} 71-51-74\n"

    # game/omake.rpy:262
    old "{b}Blood type:{/b} B\n"
    new "{b}Blood type:{/b} B\n"

    # game/omake.rpy:263
    old "{b}Favourite manga:{/b} Nanda no Ryu\n"
    new "{b}Favourite manga:{/b} Nanda no Ryu\n"

    # game/omake.rpy:264
    old "{b}Favourite videogame:{/b} Pika Pika Rocket\n"
    new "{b}Favourite videogame:{/b} Pika Pika Rocket\n"

    # game/omake.rpy:265
    old "{b}Favourite food:{/b} Chanpuruu\n"
    new "{b}Favourite food:{/b} Chanpuruu\n"

    # game/omake.rpy:266
    old "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."
    new "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."

    # game/omake.rpy:276
    old "Chapter 1"
    new "Chapter 1"

    # game/omake.rpy:276
    old "Complete the first chapter"
    new "Complete the first chapter"

    # game/omake.rpy:276
    old "Chapter 2"
    new "Chapter 2"

    # game/omake.rpy:276
    old "Complete the second chapter"
    new "Complete the second chapter"

    # game/omake.rpy:276
    old "Chapter 3"
    new "Chapter 3"

    # game/omake.rpy:276
    old "Complete the third chapter"
    new "Complete the third chapter"

    # game/omake.rpy:276
    old "Chapter 4"
    new "Chapter 4"

    # game/omake.rpy:276
    old "Complete the fourth chapter"
    new "Complete the fourth chapter"

    # game/omake.rpy:276
    old "Chapter 5"
    new "Chapter 5"

    # game/omake.rpy:276
    old "Complete the fifth chapter"
    new "Complete the fifth chapter"

    # game/omake.rpy:276
    old "Finally together"
    new "Finally together"

    # game/omake.rpy:276
    old "Finish the game for the first time"
    new "Finish the game for the first time"

    # game/omake.rpy:276
    old "The perfume of rice fields"
    new "The perfume of rice fields"

    # game/omake.rpy:276
    old "Get a kiss from Sakura"
    new "Get a kiss from Sakura"

    # game/omake.rpy:276
    old "It's not that I like you, baka!"
    new "It's not that I like you, baka!"

    # game/omake.rpy:276
    old "Get a kiss from Rika"
    new "Get a kiss from Rika"

    # game/omake.rpy:276
    old "A new kind of game"
    new "A new kind of game"

    # game/omake.rpy:276
    old "Get a kiss from Nanami"
    new "Get a kiss from Nanami"

    # game/omake.rpy:276
    old "It's a trap!"
    new "It's a trap!"

    # game/omake.rpy:276
    old "Find the truth about Sakura"
    new "Find the truth about Sakura"

    # game/omake.rpy:276
    old "Good guy"
    new "Good guy"

    # game/omake.rpy:276
    old "Tell Sakura the truth about the yukata"
    new "Tell Sakura the truth about the yukata"

    # game/omake.rpy:276
    old "It's all about the Pentiums, baby"
    new "It's all about the Pentiums, baby"

    # game/omake.rpy:276
    old "Choose to game at the beginning of the game."
    new "Choose to game at the beginning of the game."

    # game/omake.rpy:276
    old "She got me!"
    new "She got me!"

    # game/omake.rpy:276
    old "Help Rika with the festival"
    new "Help Rika with the festival"

    # game/omake.rpy:276
    old "Grope!"
    new "Grope!"

    # game/omake.rpy:276
    old "Grope Rika (accidentally)"
    new "Grope Rika (accidentally)"

    # game/omake.rpy:276
    old "A good prank"
    new "A good prank"

    # game/omake.rpy:276
    old "Prank your friends with Nanami"
    new "Prank your friends with Nanami"

    # game/omake.rpy:276
    old "I'm not little!"
    new "I'm not little!"

    # game/omake.rpy:276
    old "Help Sakura tell the truth to Nanami"
    new "Help Sakura tell the truth to Nanami"

    # game/omake.rpy:276
    old "Big change of life"
    new "Big change of life"

    # game/omake.rpy:276
    old "Complete Sakura's route"
    new "Complete Sakura's route"

    # game/omake.rpy:276
    old "City Rat"
    new "City Rat"

    # game/omake.rpy:276
    old "Complete Rika's route"
    new "Complete Rika's route"

    # game/omake.rpy:276
    old "That new girl"
    new "That new girl"

    # game/omake.rpy:276
    old "Complete Nanami's route"
    new "Complete Nanami's route"

    # game/omake.rpy:276
    old "Knee-Deep into the 34D"
    new "Knee-Deep into the 34D"

    # game/omake.rpy:276
    old "Find the secret code in the game"
    new "Find the secret code in the game"

