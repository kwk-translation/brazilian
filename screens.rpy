﻿# TODO: Translation updated at 2019-07-20 11:48

translate brazilian strings:

    # game/screens.rpy:253
    old "Back"
    new "Back"

    # game/screens.rpy:254
    old "History"
    new "History"

    # game/screens.rpy:255
    old "Skip"
    new "Skip"

    # game/screens.rpy:256
    old "Auto"
    new "Auto"

    # game/screens.rpy:257
    old "Save"
    new "Save"

    # game/screens.rpy:258
    old "Q.save"
    new "Q.save"

    # game/screens.rpy:259
    old "Q.load"
    new "Q.load"

    # game/screens.rpy:260
    old "Settings"
    new "Settings"

    # game/screens.rpy:302
    old "New game"
    new "New game"

    # game/screens.rpy:311
    old "Update Available!"
    new "Update Available!"

    # game/screens.rpy:322
    old "Load"
    new "Load"

    # game/screens.rpy:325
    old "Bonus"
    new "Bonus"

    # game/screens.rpy:331
    old "End Replay"
    new "End Replay"

    # game/screens.rpy:335
    old "Main menu"
    new "Main menu"

    # game/screens.rpy:337
    old "About"
    new "About"

    # game/screens.rpy:344
    old "Please donate!"
    new "Please donate!"

    # game/screens.rpy:347
    old "Help"
    new "Help"

    # game/screens.rpy:351
    old "Quit"
    new "Quit"

    # game/screens.rpy:585
    old "Version [config.version!t]\n"
    new "Version [config.version!t]\n"

    # game/screens.rpy:591
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"

    # game/screens.rpy:631
    old "Page {}"
    new "Page {}"

    # game/screens.rpy:631
    old "Automatic saves"
    new "Automatic saves"

    # game/screens.rpy:631
    old "Quick saves"
    new "Quick saves"

    # game/screens.rpy:673
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # game/screens.rpy:673
    old "empty slot"
    new "empty slot"

    # game/screens.rpy:690
    old "<"
    new "<"

    # game/screens.rpy:693
    old "{#auto_page}A"
    new "{#auto_page}A"

    # game/screens.rpy:696
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # game/screens.rpy:702
    old ">"
    new ">"

    # game/screens.rpy:764
    old "Display"
    new "Display"

    # game/screens.rpy:765
    old "Windowed"
    new "Windowed"

    # game/screens.rpy:766
    old "Fullscreen"
    new "Fullscreen"

    # game/screens.rpy:770
    old "Rollback Side"
    new "Rollback Side"

    # game/screens.rpy:771
    old "Disable"
    new "Disable"

    # game/screens.rpy:772
    old "Left"
    new "Left"

    # game/screens.rpy:773
    old "Right"
    new "Right"

    # game/screens.rpy:778
    old "Unseen Text"
    new "Unseen Text"

    # game/screens.rpy:779
    old "After choices"
    new "After choices"

    # game/screens.rpy:780
    old "Transitions"
    new "Transitions"

    # game/screens.rpy:790
    old "Language"
    new "Language"

    # game/screens.rpy:821
    old "Text speed"
    new "Text speed"

    # game/screens.rpy:825
    old "Auto forward"
    new "Auto forward"

    # game/screens.rpy:832
    old "Music volume"
    new "Music volume"

    # game/screens.rpy:839
    old "Sound volume"
    new "Sound volume"

    # game/screens.rpy:845
    old "Test"
    new "Test"

    # game/screens.rpy:849
    old "Voice volume"
    new "Voice volume"

    # game/screens.rpy:860
    old "Mute All"
    new "Mute All"

    # game/screens.rpy:979
    old "The dialogue history is empty."
    new "The dialogue history is empty."

    # game/screens.rpy:1044
    old "Keyboard"
    new "Keyboard"

    # game/screens.rpy:1045
    old "Mouse"
    new "Mouse"

    # game/screens.rpy:1048
    old "Gamepad"
    new "Gamepad"

    # game/screens.rpy:1061
    old "Enter"
    new "Enter"

    # game/screens.rpy:1062
    old "Advances dialogue and activates the interface."
    new "Advances dialogue and activates the interface."

    # game/screens.rpy:1065
    old "Space"
    new "Space"

    # game/screens.rpy:1066
    old "Advances dialogue without selecting choices."
    new "Advances dialogue without selecting choices."

    # game/screens.rpy:1069
    old "Arrow Keys"
    new "Arrow Keys"

    # game/screens.rpy:1070
    old "Navigate the interface."
    new "Navigate the interface."

    # game/screens.rpy:1073
    old "Escape"
    new "Escape"

    # game/screens.rpy:1074
    old "Accesses the game menu."
    new "Accesses the game menu."

    # game/screens.rpy:1077
    old "Ctrl"
    new "Ctrl"

    # game/screens.rpy:1078
    old "Skips dialogue while held down."
    new "Skips dialogue while held down."

    # game/screens.rpy:1081
    old "Tab"
    new "Tab"

    # game/screens.rpy:1082
    old "Toggles dialogue skipping."
    new "Toggles dialogue skipping."

    # game/screens.rpy:1085
    old "Page Up"
    new "Page Up"

    # game/screens.rpy:1086
    old "Rolls back to earlier dialogue."
    new "Rolls back to earlier dialogue."

    # game/screens.rpy:1089
    old "Page Down"
    new "Page Down"

    # game/screens.rpy:1090
    old "Rolls forward to later dialogue."
    new "Rolls forward to later dialogue."

    # game/screens.rpy:1094
    old "Hides the user interface."
    new "Hides the user interface."

    # game/screens.rpy:1098
    old "Takes a screenshot."
    new "Takes a screenshot."

    # game/screens.rpy:1102
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # game/screens.rpy:1108
    old "Left Click"
    new "Left Click"

    # game/screens.rpy:1112
    old "Middle Click"
    new "Middle Click"

    # game/screens.rpy:1116
    old "Right Click"
    new "Right Click"

    # game/screens.rpy:1120
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Mouse Wheel Up\nClick Rollback Side"

    # game/screens.rpy:1124
    old "Mouse Wheel Down"
    new "Mouse Wheel Down"

    # game/screens.rpy:1131
    old "Right Trigger\nA/Bottom Button"
    new "Right Trigger\nA/Bottom Button"

    # game/screens.rpy:1135
    old "Left Trigger\nLeft Shoulder"
    new "Left Trigger\nLeft Shoulder"

    # game/screens.rpy:1139
    old "Right Shoulder"
    new "Right Shoulder"

    # game/screens.rpy:1143
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # game/screens.rpy:1147
    old "Start, Guide"
    new "Start, Guide"

    # game/screens.rpy:1151
    old "Y/Top Button"
    new "Y/Top Button"

    # game/screens.rpy:1154
    old "Calibrate"
    new "Calibrate"

    # game/screens.rpy:1219
    old "Yes"
    new "Yes"

    # game/screens.rpy:1220
    old "No"
    new "No"

    # game/screens.rpy:1266
    old "Skipping"
    new "Skipping"

    # game/screens.rpy:1487
    old "Menu"
    new "Menu"

    # game/screens.rpy:1541
    old "Musics and pictures"
    new "Musics and pictures"

    # game/screens.rpy:1544
    old "Picture gallery"
    new "Picture gallery"

    # game/screens.rpy:1548
    old "Music player"
    new "Music player"

    # game/screens.rpy:1550
    old "Opening song lyrics"
    new "Opening song lyrics"

    # game/screens.rpy:1553
    old "Achievements"
    new "Achievements"

    # game/screens.rpy:1559
    old "Characters profiles"
    new "Characters profiles"

    # game/screens.rpy:1580
    old "Bonus chapters"
    new "Bonus chapters"

    # game/screens.rpy:1583
    old "1 - A casual day at the club"
    new "1 - A casual day at the club"

    # game/screens.rpy:1588
    old "2 - Questioning sexuality (Sakura's route)"
    new "2 - Questioning sexuality (Sakura's route)"

    # game/screens.rpy:1593
    old "3 - Headline news"
    new "3 - Headline news"

    # game/screens.rpy:1598
    old "4a - A picnic at the summer (Sakura's route)"
    new "4a - A picnic at the summer (Sakura's route)"

    # game/screens.rpy:1603
    old "4b - A picnic at the summer (Rika's route)"
    new "4b - A picnic at the summer (Rika's route)"

    # game/screens.rpy:1608
    old "4c - A picnic at the summer (Nanami's route)"
    new "4c - A picnic at the summer (Nanami's route)"

    # game/screens.rpy:1625
    old "Max le Fou - Taichi's Theme"
    new "Max le Fou - Taichi's Theme"

    # game/screens.rpy:1627
    old "Max le Fou - Sakura's Waltz"
    new "Max le Fou - Sakura's Waltz"

    # game/screens.rpy:1629
    old "Max le Fou - Rika's theme"
    new "Max le Fou - Rika's theme"

    # game/screens.rpy:1631
    old "Max le Fou - Of Bytes and Sanshin"
    new "Max le Fou - Of Bytes and Sanshin"

    # game/screens.rpy:1633
    old "Max le Fou - Time for School"
    new "Max le Fou - Time for School"

    # game/screens.rpy:1635
    old "Max le Fou - Sakura's secret"
    new "Max le Fou - Sakura's secret"

    # game/screens.rpy:1637
    old "Max le Fou - I think I'm in love"
    new "Max le Fou - I think I'm in love"

    # game/screens.rpy:1639
    old "Max le Fou - Darkness of the Village"
    new "Max le Fou - Darkness of the Village"

    # game/screens.rpy:1641
    old "Max le Fou - Love always win"
    new "Max le Fou - Love always win"

    # game/screens.rpy:1643
    old "Max le Fou - Ondo"
    new "Max le Fou - Ondo"

    # game/screens.rpy:1645
    old "Max le Fou - Happily Ever After"
    new "Max le Fou - Happily Ever After"

    # game/screens.rpy:1647
    old "J.S. Bach - Air Orchestral suite #3"
    new "J.S. Bach - Air Orchestral suite #3"

    # game/screens.rpy:1649
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new "Mew Nekohime - TAKE MY HEART (TV Size)"

